// Copyright (c) 2024 Deltaman group limited. All rights reserved.
// $Id$

package com.deltaxml.samples;

import com.deltaxml.cores9api.DocumentComparator;
import com.deltaxml.cores9api.DocumentComparator.ExtensionPoint;
import com.deltaxml.cores9api.FilterChain;
import com.deltaxml.cores9api.FilterStepHelper;
import com.deltaxml.cores9api.config.LexicalPreservationConfig;
import com.deltaxml.cores9api.config.ModifiedWhitespaceBehaviour;
import com.deltaxml.cores9api.config.PresetPreservationMode;
import com.deltaxml.cores9api.config.ResultReadabilityOptions;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.sf.saxon.s9api.Serializer;

import java.io.File;
import java.io.IOException;
import java.net.URL;


public class DiffReportServletDC extends HttpServlet {

  public void init(ServletConfig config) throws ServletException {
    super.init(config);
    // Hardwire Saxon of XSLT processing and Xerces for SAX, included in .war file
    System.setProperty("javax.xml.parsers.SAXParserFactory", "org.apache.xerces.jaxp.SAXParserFactoryImpl");
    System.setProperty("javax.xml.transform.TransformerFactory", "net.sf.saxon.TransformerFactoryImpl");
  }

  public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
    doPost(req, res);
  }

  public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
    try {
      log("DiffReportServlet Request from host: " + req.getRemoteHost());
      log("DiffReportServlet Request from IP: " + req.getRemoteAddr());
      log("DiffReportServlet User Agent: " + req.getHeader("User-Agent"));

      // The form has 6 params, 4 checkboxes and 2 textareas for specifying systemIds
      boolean wbw= req.getParameter("wbw") == null ? false : req.getParameter("wbw").equalsIgnoreCase("Y");
      boolean norm= req.getParameter("norm") == null ? false : req.getParameter("norm").equalsIgnoreCase("Y");
      boolean valid= req.getParameter("valid") == null ? false : req.getParameter("valid").equalsIgnoreCase("Y");
      boolean plain= req.getParameter("plain") == null ? false : req.getParameter("plain").equalsIgnoreCase("Y");
      URL in1= new URL(req.getParameter("sysid1"));
      URL in2= new URL(req.getParameter("sysid2"));

      DocumentComparator comparator= new DocumentComparator();

      FilterStepHelper fsHelper= comparator.newFilterStepHelper();
      FilterChain infilters= fsHelper.newFilterChain();
      FilterChain outfilters= fsHelper.newFilterChain();

      comparator.setOutputProperty(Serializer.Property.INDENT, "yes");

      // By default word by word comparison is performed on all elements
      ResultReadabilityOptions resultReadabilityOptions= comparator.getResultReadabilityOptions();

      resultReadabilityOptions.setElementSplittingEnabled(false);
      resultReadabilityOptions.setOrphanedWordDetectionEnabled(false);
      
      LexicalPreservationConfig lexConfig= new LexicalPreservationConfig(PresetPreservationMode.ROUND_TRIP);
      
      // order of following statements is important so that filters are added in correct order to Lists
      if (norm) {
        resultReadabilityOptions.setModifiedWhitespaceBehaviour(ModifiedWhitespaceBehaviour.NORMALIZE);
        lexConfig.setPreserveIgnorableWhitespace(false);
      }
      comparator.setLexicalPreservationConfig(lexConfig);

      ServletContext context= req.getSession().getServletContext();
      if (!wbw) {
        String fullPath= context.getRealPath("/WEB-INF/lib/disable-word-by-word.xsl");
        infilters.addStep(fsHelper.newFilterStep(new File(fullPath), "disable-word-by-word"));
        comparator.setExtensionPoint(ExtensionPoint.PRE_FLATTENING, infilters);
      }

      if (!plain) {
        // the VERSION property below is for HTML5 and is only supported by Saxon 9.5
        comparator.setOutputProperty(Serializer.Property.VERSION, "5.0");
        // The folding html filter is contained in deltaxml.jar, the following should give us
        // a URL to use as one of the support List objects for filter. The URL will be a jar-url like this:
        // jar:file:..../apache-tomcat-5.5.17/webapps/DiffReport/WEB-INF/lib/deltaxml.jar!/xsl/dx2-deltaxml-folding-html.xsl
        comparator.setOutputProperty(Serializer.Property.METHOD, "html");
        outfilters.addStep(fsHelper.newFilterStepFromResource("/xsl/dx2-deltaxml-folding-html.xsl", "dx2-deltaxml-folding-html"));
      } else {
        comparator.setOutputProperty(Serializer.Property.METHOD, "xml");
      }
      comparator.setParserFeature("http://xml.org/sax/features/validation", valid);
      comparator.setExtensionPoint(ExtensionPoint.OUTPUT_FINAL, outfilters);

      try {
        comparator.compare(in1, in2, res.getOutputStream());
      } catch (Error e) { // handles errors via ServletExceptions and thus error.jsp
        if (e.getCause() != null) {
          throw new ServletException(e.getCause());
        } else {
          throw new ServletException(e);
        }
      }
      log("DiffReportServlet should have completed normally");
    } catch (Exception e) {
      log("exception in doPost", e);
      req.setAttribute("exception", e);
      req.getRequestDispatcher("/error.jsp").forward(req, res);
    }
  }
}
