// Copyright (c) 2024 Deltaman group limited. All rights reserved.
// $Id$
package com.deltaxml.samples;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Tests the servlet by firing multiple concurrent posts to the DeltaXML test servlet. This was used as a simple proof-of-concept
 * that the API could be used in multithreaded environments. It fires 100 threads each doing a post to the servlet; if the server
 * has the capacity these could execute concurrently. The posted, input files are hardwired in the code, the output files are
 * named according to the thread number. Since the inputs and pipeline are identical so should the output files and therefore
 * checking/comparing the outputs could be used to check the correctness of the code and server.
 */
public class ServletTester {

  /**
   * @param args unused
   */
  public static void main(String[] args) {
    for (int i= 0; i < 100; i++) {
      Thread current= new Thread() {
        @Override
        public void run() {
          PostMethod pm= new PostMethod("http://localhost:8080/DiffReport/request");
          pm.addParameter("plain", "Y");
          pm.addParameter("norm", "Y");
          pm.addParameter("wbw", "Y");
          pm.addParameter("sysid1", "file:///Users/nigelw/dev/core/samples/AddressBook/build.xml");
          pm.addParameter("sysid2", "file:///Users/nigelw/dev/core/samples/WordByWord/build.xml");
          HttpClient c= new HttpClient();
          try {
            c.executeMethod(pm);
          } catch (Exception e) { // HttpException or IOException
            throw new Error("Failed to execute post for " + this.getName(), e);
          }
          byte[] result= pm.getResponseBody();
          FileOutputStream fos;
          try {
            fos= new FileOutputStream(new File("result-" + this.getName() + ".xml"));
            fos.write(result);
            fos.close();
          } catch (Exception e) { // FileNotFoundException or IOException
            throw new Error("Failed to write result file for thread: " + this.getName(), e);
          }
        }
      };
      current.setName("thread" + i);
      current.start();
    }
  }
}
