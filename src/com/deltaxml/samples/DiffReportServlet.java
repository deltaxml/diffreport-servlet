// Copyright (c) 2024 Deltaman group limited. All rights reserved.
// $Id$

package com.deltaxml.samples;

import com.deltaxml.cores9api.FilterChain;
import com.deltaxml.cores9api.FilterStepHelper;
import com.deltaxml.cores9api.PipelinedComparatorS9;
import com.deltaxml.pipe.filters.NormalizeSpace;
import com.deltaxml.pipe.filters.dx2.wbw.WordInfilter;
import com.deltaxml.pipe.filters.dx2.wbw.WordOutfilter;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.net.URL;


public class DiffReportServlet extends HttpServlet {

  public void init(ServletConfig config) throws ServletException {
    super.init(config);
    // Hardwire Saxon of XSLT processing and Xerces for SAX, included in .war file
    System.setProperty("javax.xml.parsers.SAXParserFactory", "org.apache.xerces.jaxp.SAXParserFactoryImpl");
    System.setProperty("javax.xml.transform.TransformerFactory", "net.sf.saxon.TransformerFactoryImpl");
  }

  public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
    doPost(req, res);
  }

  public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
    try {
      log("DiffReportServlet Request from host: " + req.getRemoteHost());
      log("DiffReportServlet Request from IP: " + req.getRemoteAddr());
      log("DiffReportServlet User Agent: " + req.getHeader("User-Agent"));

      // The form has 6 params, 4 checkboxes and 2 textareas for specifying systemIds
      boolean wbw= req.getParameter("wbw") == null ? false : req.getParameter("wbw").equalsIgnoreCase("Y");
      boolean norm= req.getParameter("norm") == null ? false : req.getParameter("norm").equalsIgnoreCase("Y");
      boolean valid= req.getParameter("valid") == null ? false : req.getParameter("valid").equalsIgnoreCase("Y");
      boolean plain= req.getParameter("plain") == null ? false : req.getParameter("plain").equalsIgnoreCase("Y");
      URL in1= new URL(req.getParameter("sysid1"));
      URL in2= new URL(req.getParameter("sysid2"));

      PipelinedComparatorS9 pc= new PipelinedComparatorS9();
      FilterStepHelper fsHelper= pc.newFilterStepHelper();
      FilterChain infilters= fsHelper.newFilterChain();
      FilterChain outfilters= fsHelper.newFilterChain();
      // order of following statements is important so that filters are added in correct order to Lists
      if (norm) {
        infilters.addStep(fsHelper.newFilterStep(NormalizeSpace.class, "NormalizeSpace"));
      }
      if (wbw) {
        infilters.addStep(fsHelper.newFilterStep(WordInfilter.class, "WordInfilter"));
        outfilters.addStep(fsHelper.newFilterStep(WordOutfilter.class, "WordOutfilter"));
      }
      if (!plain) {
        // The folding html filter is contained in deltaxml.jar, the following should give us
        // a URL to use as one of the support List objects for filter. The URL will be a jar-url like this:
        // jar:file:..../apache-tomcat-5.5.17/webapps/DiffReport/WEB-INF/lib/deltaxml.jar!/xsl/dx2-deltaxml-folding-html.xsl
        URL htmlfilter= this.getClass().getClassLoader().getResource("xsl/dx2-deltaxml-folding-html.xsl");
        outfilters.addStep(fsHelper.newFilterStep(htmlfilter, "dx2-deltaxml-folding-html"));
        res.setContentType("text/html");
      } else {
        res.setContentType("application/xml");
      }
      pc.setInputFilters(infilters);
      pc.setOutputFilters(outfilters);
      pc.setComparatorFeature("http://deltaxml.com/api/feature/isFullDelta", true);
      pc.setParserFeature("http://xml.org/sax/features/validation", valid);
      try {
        pc.compare(in1, in2, res.getOutputStream());
      } catch (Error e) { // handles errors via ServletExceptions and thus error.jsp
        if (e.getCause() != null) {
          throw new ServletException(e.getCause());
        } else {
          throw new ServletException(e);
        }
      }
      log("Dif fReportServlet should have completed normally");
    } catch (Exception e) {
      log("exception in doPost", e);
      req.setAttribute("exception", e);
      req.getRequestDispatcher("/error.jsp").forward(req, res);
    }
  }
}
