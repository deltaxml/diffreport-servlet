<%@ page isErrorPage ="true" %>
 <html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    </head>
    <body>
      <h1>DiffReport Sample Servlet Processing Error</h1>

      <p>This sample servlet does not have sophisticated error handling - sorry!
      Please try and interpret and recover using the information in the stack-trace
      below and/or your servlet/app-server log files.</p>

      <p>There was an error processing your data. Please press the back-button on
         your browser and correct your data and/or options.  If you believe
         there is a problem with the Sample Servlet please 
         <a href="http://www.deltaxml.com/contact/">contact us</a> and we'll check it.
         If possible include any tracebacks which may be provided below: </p> 
      <hr />
      <pre><%
        Exception e2 = (Exception) request.getAttribute ("exception");
        if (e2 != null) {
          e2.printStackTrace(new java.io.PrintWriter(out));
        }
      %></pre>
  </body>
</html>
