# DiffReport Sample Servlet

An example servlet showing how XML Compare could be used and deployed in a servlet engine or Jakarta EE application server. Also serves as a multithreaded test case.

## Introduction
This sample code demonstrates how DeltaXML pipelines can be used in servlets. The code in this sample is used to produce a web app and this has been tested with:

- Apache Tomcat 10.1.8
- IBM WebSphere 7.0

The rest of the sample documentation will discuss how to deploy and test the code using Apache Tomcat as this is both easily available and simple to deploy and use.  Other servlet engines or application servers could also be used, however we have not done any extensive testing. If you do use another system please do let us know and we will update the information in this page.  The sample can be found in the samples/DiffReportServlet directory in your release.
Please note that this sample is not intended to demonstrate good servlet development/coding techniques!
## Code Details
This sample subdirectory contains three Java files. Two of these files (DiffReportServlet.java ) and (DiffReportServletDC.java ) handles the servlet request/response processing using the Pipelined Comparator and Document Comparator respectively. The servlet itself can be used interactively in a browser, or alternatively a Java program is provided to run and test the servlet (ServletTester.java ). The test program was initially developed to test that DeltaXML operated correctly in a multi-threaded environment and could be extended or adapted for similar purposes. 

** Please note: This test program is not a realistic way of deploying or using the servlet, it was written specifically as a concurrent 'stress test'.**
## Build and Deploy
The build.xml ant script can be used to create DiffReport.war using the Pipelined Comparator and DiffReportDC.war using the Document Comparator. These can be deployed in your favourite servlet engine or App server. The script contains a 'servlet.jar' property which will need to be adjusted to match the location of this API appropriate to the software that you are using (this is only required at compile time).
To produce the servlet run:

	ant dist
	
There are a number of ways of deploying a servlet, we will use the manual approach with Tomcat.  The following example is based on a Unix system where the CATALINA_HOME environment variable corresponds to the Tomcat install location (its actual value on our systems is: /usr/local/java/apache-tomcat-8.0.20).
To deploy the servlet it is first copied to the webapps subfolder:

	cp DiffReport.war $CATALINA_HOME/webapps
	
Or

	cp DiffReportDC.war $CATALINA_HOME/webapps
	
Then Tomcat can be started or restarted, for example:

	$CATALINA_HOME/bin/catalina.sh start
	
After which the servlet should be available for use in a browser.  If you are using an 'out of the box' Tomcat installation this will be at:

`http://localhost:8080/DiffReport/`

Or

`http://localhost:8080/DiffReportDC/`

The page includes buttons for pipeline configuration and two text boxes where URLs can be used to specify the comparison inputs. We include two example URLs that can be copied and pasted into the text boxes, but when doing so please be careful not to add any extra whitespace as the servlet does not have any request handling code to deal with it.
## Command-line Client and Test Harness
A simple driver has been developed, initially intended to prove that DeltaXML pipelines would operate in a multi-threaded environment. This code uses the Apache HTTP client library to provide servlet requests and handle the responses. The code creates 100 threads each of which specifies the inputs (as file URLs) and handles the responses saving the results into a file for every thread used. As the inputs and pipeline used are identical the 100 result files should also be identical and this can be used as a simple test of multi-threaded operation. In order to use the code, the build.xml ant script will need to be configured to point to local copies (not supplied by DeltaXML) of the Apache http client and logging jar files. The source code will also need to be modified to point to XML files available locally (file: URIs or XML files on a local http server). After doing this the test can be compiled and run, using the following target:

	ant test
	
After running the target you should find 100 identical files in the sample subdirectory, for example:

	$ ls -l result-thread*.xml
	-rw-r--r--  1 nigelw  staff  1274 Dec  3 12:45 result-thread0.xml
	-rw-r--r--  1 nigelw  staff  1274 Dec  3 12:45 result-thread1.xml
	-rw-r--r--  1 nigelw  staff  1274 Dec  3 12:45 result-thread10.xml
	-rw-r--r--  1 nigelw  staff  1274 Dec  3 12:45 result-thread11.xml
	-rw-r--r--  1 nigelw  staff  1274 Dec  3 12:45 result-thread12.xml
	...
	-rw-r--r--  1 nigelw  staff  1274 Dec  3 12:45 result-thread98.xml
	-rw-r--r--  1 nigelw  staff  1274 Dec  3 12:45 result-thread99.xml
	
## Warning Notes
- We should point out the following warnings if you are considering extending this code:
- As mentioned previously we don't consider this to be a good example of servlet programming.
- We have developed this code on a Unix system and have not tested it on Windows or other operating systems. Some porting work may be needed, particularly for the test harness and ant build script, if you wish to use other systems.
- While the 100 comparison results are generated very quickly for these small examples the code could be optimized, in particular performing the pipeline setup in addition to the compare method in each doPost method is not efficient.  More efficient code could for example use a ThreadPool of where a number of PipelinedComparatorS9 instances are pre-configured as part of the init or other initialization process and where the doPost would just call the compare methods. Please note the multithreading constraints from the Javadoc - a single PipelinedComparator or PipelinedComparatorS9 should not be used by multiple threads concurrently, but different PipelinedComparator or PipelinedComparatorS9 instances may be used concurrently by different threads